# Lab Control
A MQTT based lab control system for various High-Energy Physics electronics. 

## Architecture description
Lab control consists of various components, that are explained in the following. More detailed
info with a description of the configuration and installation will follow soon.

### Agents
For each device or communication path (e.g. all VME modules must be controlled by one agent to
safely prevent multiple threads accessing the VME bridge simultaneously) there is an agent
listening to corresponding MQTT topics to change the configuration of the device. The same agent
periodically updates MQTT topics with the current state read back from the device (e.g. the
measured voltage for each channel of a power supply). The structure of the agents is to be
refactored as it grew quickly to be ready in time for a testbeam and is not optimal yet.

### Web Interface
The web interface is written using Flask and uses Jinja2 templates for rendering the various
devices. It communicates with the MQTT broker via a websocket bridge and hence receives all updates
from MQTT directly. This allows for the web interface to be open on multiple PCs and have it synced
at all times.

### MQTT Broker
For the MQTT broker we use Eclipse Mosquitto running from a docker container. It is providing
connections via MQTT or via websockets (the latter is needed to talk to it from the browser). The
setup is as trivial as just starting up the container. A config file for it will be added in a
future update.

### Logging (optional)
Optionally it is possible to have a logging setup running. For that we use InfluxDB and a custom
MQTT bridge to create the entries in influx written in python. To display the logged data Grafana
is used. All of these run in a docker and a docker-compose file will be provided in a future
update.
