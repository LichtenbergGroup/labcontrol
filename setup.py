import setuptools

setuptools.setup(
    name="labcontrol",
    version="0.0.3",
    author="Friedemann Neuhaus",
    author_email="friedemann@neuhaus-tech.de",
    description="Lab control system",
    url="https://gitlab.cern.ch/LichtenbergGroup/labcontrol",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    include_package_data=True,
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=[
        "pyyaml",
        "flask",
        "paho-mqtt",
        "pyvme @ git+https://github.com/fneuhaus/pyvme",
        "pycaen @ git+https://github.com/fneuhaus/pycaen",
        "amptek-mini-x @ git+https://github.com/fneuhaus/amptek-mini-x",
    ],
)
