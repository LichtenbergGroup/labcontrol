from pathlib import Path
from time import sleep
from yaml import load
import logging
from argparse import ArgumentParser

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from .agent import AgentProcess
from . import agents as available_agents


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(name)s] %(levelname)s: %(message)s",
)


def main():
    argparser = ArgumentParser()
    argparser.add_argument(
        "-c",
        "--config",
        dest="config_filename",
        help="Path to the configuration file",
        type=Path,
        default=Path.home() / ".config/labcontrol/config.yml"
    )
    argparser.add_argument(
        "-v",
        "--verbose",
        help="Set log level to DEBUG",
        action="store_true",
    )
    args = argparser.parse_args()
    if not args.config_filename.is_file():
        print("There is no configuration file for lab control.")
        exit(1)

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    logger = logging.getLogger("main")
    with args.config_filename.open() as f:
        config = load(f, Loader=Loader)

    agent_processes = []
    for device_id, device_config in config["devices"].items():
        if not device_config.get("run_agent", True):
            continue
        try:
            agent_class = getattr(available_agents, device_config["type"])
        except AttributeError:
            logging.error(
                f"The agent with type {device_config['type']} could not be loaded. Maybe some dependency is missing?"
            )
            continue
        agent_processes.append(AgentProcess(agent_class, device_id, config))

    # Start the agent processes
    for agent_process in agent_processes:
        agent_process.start()

    try:
        while True:
            # Agents are running in their own processes, just monitor if they are still alive and
            # restart if necessary
            for agent_process in agent_processes:
                if not agent_process.is_alive():
                    logger.warning(
                        f"Agent {agent_process.device_id} is not active anymore!"
                    )
                    agent_process.restart()

            sleep(1)
    except KeyboardInterrupt:
        print("Shutting down")
        for agent_process in agent_processes:
            agent_process.stop()


if __name__ == "__main__":
    main()
