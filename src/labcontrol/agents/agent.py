#!/usr/bin/env python3
import logging
import multiprocessing as mp
from time import sleep, time
import paho.mqtt.client as mqtt
import signal


class Agent:
    DEFAULT_UPDATE_INTERVAL = 2

    def __init__(self, device_id, config):
        self._device_id = device_id
        self._config = config
        self._device_config = config["devices"][device_id]
        self._mqtt = None

    def _mqtt_connect(self):
        self._mqtt = mqtt.Client()
        self._mqtt.on_connect = self.on_connect
        self._mqtt.on_message = self.on_message
        self._mqtt.connect(
            self._config["mqtt"]["hostname"], self._config["mqtt"]["port"]
        )
        self._mqtt.will_set(
            f"{self._device_id}/status", payload="inactive", retain=True
        )
        self._mqtt.publish(f"{self._device_id}/status", payload="active", retain=True)

    def _mqtt_disconnect(self):
        if not self._mqtt or not self._mqtt.is_connected():
            return
        self._mqtt.on_message = None
        self._mqtt.loop_start()
        self._mqtt.publish(
            f"{self._device_id}/status", payload="inactive", retain=True
        ).wait_for_publish()
        self._mqtt.disconnect()
        self._mqtt.loop_stop()

    def connect(self):
        try:
            self._mqtt_connect()
        except ConnectionRefusedError:
            self._mqtt = None
            raise AgentError("The MQTT server is not available")

    def disconnect(self):
        self._mqtt_disconnect()

    def on_connect(self, client, userdata, flags, rc):
        pass

    def on_message(self, userdata, msg):
        pass

    @property
    def is_connected(self):
        return self._mqtt is not None

    def publish(self, topic, message, qos=0, retain=False, raw_topic=False):
        if not raw_topic:
            topic = f"{self._device_id}/{topic}"
        self._mqtt.publish(topic, message, qos=qos, retain=retain)

    @property
    def name(self):
        return type(self)

    @property
    def update_interval(self):
        return self._device_config.get("interval", Agent.DEFAULT_UPDATE_INTERVAL)


class AgentError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class VMEAgent(Agent):
    controller = None

    def __init__(self, device_id, config, controller=None):
        if controller is None and VMEAgent.controller is None:
            from pyvme import V2718

            VMEAgent.controller = V2718()
        super().__init__(device_id, config)
        if controller is None:
            self._controller = VMEAgent.controller
        else:
            self._controller = controller


class USBAgent(Agent):
    def __init__(self, device_id, config):
        super().__init__(device_id, config)


class AgentProcess:
    """
    Wrap the agent within it's own Process to allow asynchronous update of the various agents in a
    single program. This also helps with respawning crashes agents.
    """

    def __init__(self, agent_class, device_id, config, *args, **kwargs):
        self.__agent_class = agent_class
        self.device_id = device_id
        self.update_interval = config["devices"][device_id].get(
            "interval", Agent.DEFAULT_UPDATE_INTERVAL
        )
        self.__last_update = mp.Value("d", time())
        self.__keep_running = mp.Value("b", True)
        self.__config = config
        self.__args = args
        self.__kwargs = kwargs
        self.__logger = logging.getLogger(device_id)
        self.process = None

    def start(self):
        self.__logger.info("Starting agent")
        self.__keep_running.value = True
        self.process = mp.Process(
            target=self.main,
            args=[self.__agent_class, self.device_id, self.__config, *self.__args],
            kwargs=self.__kwargs,
            daemon=True,
        )
        self.process.start()

    def stop(self):
        self.__logger.info("Stopping agent. Give it 10 seconds to end gracefully")
        self.__keep_running.value = False
        self.process.join(10)
        if self.process.is_alive():
            self.__logger.error("Agent did not stop by itself. Terminating it now!")
            self.process.terminate()

    def restart(self):
        self.__logger.info("Restarting agent.")
        self.stop()
        self.start()

    def is_alive(self):
        """
        Check if the process is running and if the process is not hanging for more than 3x the
        update interval of the agent.
        """
        return (
            self.process is not None
            and self.process.is_alive()
            and time() - self.__last_update.value < 3 * self.update_interval
        )

    def main(self, agent_class, device_id, config, *args, **kwargs):
        """
        Instantiate the agent and run the updates.
        """
        logger = logging.getLogger(device_id)

        # Ignore SIGINT (KeyboardInterrupt) as this is handled by the main thread
        signal.signal(signal.SIGINT, signal.SIG_IGN)

        # Initialize the agent and keep it running
        agent = agent_class(device_id, config, *args, **kwargs)
        logger.debug(f"Initialized agent for {agent._device_id}")
        perform_full_update = True
        mqtt_loop_timeout = min(5, agent.update_interval)
        while self.__keep_running.value:
            self.__last_update.value = time()
            if not agent.is_connected:
                try:
                    logger.debug("Try to connect")
                    agent.connect()
                    perform_full_update = True
                except AgentError:
                    logger.debug("Could not connect, retrying in 1 second.")
                    sleep(1)
                continue

            # Look for incoming message until we need to send the next update
            next_update = time() + agent.update_interval
            while self.__keep_running.value and time() < next_update:
                try:
                    agent._mqtt.loop(timeout=mqtt_loop_timeout)
                except Exception as e:
                    logger.error(f"Exception in mqtt.loop: {str(e)}")
                    agent.disconnect()
                    break

            # Handle update
            try:
                agent.update(initial=perform_full_update)
                perform_full_update = False
                logger.debug("Update done")
            except AgentError as e:
                perform_full_update = True
                logger.info(f"An error occured during update: {str(e)}")

        agent.disconnect()
