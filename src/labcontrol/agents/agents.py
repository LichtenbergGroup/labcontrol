try:
    from .caen import CaenV6533, CaenV895, Caen1471
except ImportError:
    pass


try:
    from .mks import MKSPR4000
except ImportError:
    pass


try:
    from .custom import LichtenlabTrigger
except ImportError:
    pass


try:
    from .amptek import MiniX
except ImportError:
    pass


try:
    from .srs import SRS
except ImportError:
    pass
