#!/usr/bin/env python3
from .agent import Agent, AgentError
import minix


class MiniX(Agent):
    def __init__(self, device_id, config):
        super().__init__(device_id, config)
        self._device = None

    def connect(self):
        try:
            self._device = minix.MiniX(self._device_config["serial_number"])
        except ConnectionError:
            self._device = None
            raise AgentError(f"Could not find {self._device_config['serial_number']}")
        super().connect()

    def disconnect(self):
        self._device._ftdi.close()
        del self._device
        self._device = None
        super().disconnect()

    @property
    def is_connected(self):
        return super().is_connected and self._device is not None and self._device._ftdi is not None and self._device._ftdi.is_connected

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/+/set")

    def on_message(self, client, userdata, msg):
        if not self.is_connected:
            return
        _, prop, set_ = msg.topic.split("/")
        if set_ != "set":
            return
        if prop not in ["voltage", "current", "enabled"]:
            return
        try:
            value = float(msg.payload)
        except ValueError:
            print(f"Could not parse float from {msg.payload}")
            return

        if prop == "voltage":
            self._device.voltage = value
            new_value = value
        elif prop == "current":
            self._device.current = value
            new_value = value
        elif prop == "enabled":
            try:
                if value == 0:
                    self._device.power_off()
                else:
                    self._device.power_on()
            except Exception as e:
                print(e)
                pass
            new_value = int(self._device.enabled)
        if new_value != value:
            self.publish(msg.topic.replace("/set", ""), new_value, raw_topic=True)

    def update(self, initial=False):
        if not self.is_connected:
            raise AgentError("Device is not connected")

        try:
            self.publish("monitor_ready", int(self._device.monitor_ready))
            self.publish("interlock_state", int(self._device.interlock_state))
            self.publish("temperature", self._device.temperature)
            self.publish("measured_voltage", self._device.measured_voltage)
            self.publish("measured_current", self._device.measured_current)
            if self._device.enabled:
                print(
                    f"Voltage: {self._device.measured_voltage}, Current: {self._device.measured_current}"
                )
            if initial:
                self.publish("voltage", self._device.voltage, retain=True)
                self.publish("current", self._device.current, retain=True)
                self.publish("enabled", int(self._device.enabled), retain=True)
        except minix.NotConnected:
            del self._device
            self._device = None
            raise AgentError("Device is not connected")
