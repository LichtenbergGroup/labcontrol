#!/usr/bin/env python3
import re
import pyvme
import pycaen
from serial.serialutil import SerialException
from .agent import AgentError, VMEAgent, USBAgent


class CaenV6533(VMEAgent):
    SET_RE = re.compile(r".*\/channel([0-5])\/(.*)/set")
    ALLOWED_PROPERTIES = [
        "enabled",
        "voltage",
        "current_limit",
        "voltage_limit",
        "trip_time",
        "ramp_down_rate",
        "ramp_up_rate",
    ]

    def __init__(self, device_id, config, vme_controller=None):
        super().__init__(device_id, config, vme_controller)

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/#")

    def on_message(self, client, userdata, msg):
        if not (m := self.SET_RE.match(msg.topic)):
            return
        ch, prop = m.groups()
        try:
            value = float(msg.payload)
        except ValueError:
            return

        if prop not in self.ALLOWED_PROPERTIES:
            return

        retain = False
        with pyvme.modules.V6533(
            self._controller, self._device_config["vme_address"]
        ) as hv:
            channel = hv.channels[int(m.group(1))]
            setattr(channel, prop, value)
            new_value = getattr(channel, prop)
            if new_value is False:
                new_value = 0
            elif new_value is True:
                new_value = 1
        if prop == "enabled":
            retain = True
        self.publish(
            msg.topic.replace("/set", ""), new_value, raw_topic=True, retain=retain
        )

    def update(self, initial=False):
        with pyvme.modules.V6533(
            self._controller, self._device_config["vme_address"]
        ) as hv:
            p = {}
            for ch in hv.channels:
                chi = ch.channel
                if initial:
                    self.publish(f"channel{chi}/enabled", int(ch.enabled), retain=True)
                    self.publish(f"channel{chi}/voltage", ch.voltage, retain=True)
                    self.publish(
                        f"channel{chi}/current_limit", ch.current_limit, retain=True
                    )
                    self.publish(
                        f"channel{chi}/voltage_limit", ch.voltage_limit, retain=True
                    )
                    self.publish(f"channel{chi}/trip_time", ch.trip_time, retain=True)
                    self.publish(
                        f"channel{chi}/ramp_down_rate",
                        ch.ramp_down_rate,
                        retain=True,
                    )
                    self.publish(
                        f"channel{chi}/ramp_up_rate", ch.ramp_up_rate, retain=True
                    )
                    self.publish(f"channel{chi}/polarity", ch.polarity, retain=True)
                self.publish(f"channel{chi}/measured_voltage", ch.measured_voltage)
                self.publish(f"channel{chi}/measured_current", ch.measured_current)


class CaenV895(VMEAgent):
    SET_CHANNEL_RE = re.compile(r".*\/channel([0-9]|1[0-5])\/(.*)/set")
    SET_MODULE_RE = re.compile(r".*\/(.*)/set")

    def __init__(self, device_id, config, vme_controller=None):
        super().__init__(device_id, config, vme_controller)

    def set_defaults(self):
        defaults = self._device_config.get("defaults", {})
        inhibit_pattern = 0
        with pyvme.modules.V895(
            self._controller, self._device_config["vme_address"]
        ) as mod:
            for channel, props in defaults.get("channels", {}).items():
                if props.get("threshold"):
                    mod.set_threshold(int(channel), int(props.get("threshold")))
                    self.publish(f"channel{channel}/threshold", props.get("threshold"))
                if props.get("enabled"):
                    inhibit_pattern |= (1 << int(channel))
            mod.set_inhibit_pattern(inhibit_pattern)
            self.publish("inhibit_pattern", inhibit_pattern)

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/#")
        self.set_defaults()

    def on_message(self, client, userdata, msg):
        if m := self.SET_CHANNEL_RE.match(msg.topic):
            # Per channel settings
            ch, prop = m.groups()
        elif m := self.SET_MODULE_RE.match(msg.topic):
            # Module settings
            prop = m.groups()[0]
        else:
            return

        try:
            value = float(msg.payload)
        except ValueError:
            print("ValueError")
            return

        with pyvme.modules.V895(
            self._controller, self._device_config["vme_address"]
        ) as mod:
            if prop == "threshold":
                if value == 0:
                    return
                try:
                    mod.set_threshold(int(ch), int(value))
                except ValueError:
                    self.publish(f"channel{ch}/threshold", 0)
            elif prop == "inhibit_pattern":
                value = int(value)
                if value < 0:
                    return
                try:
                    mod.set_inhibit_pattern(value)
                    self.publish("inhibit_pattern", value)
                except ValueError:
                    self.publish("inhibit_pattern", -1)
            elif prop == "output_width":
                try:
                    value = int(value)
                    if not 0 <= value <= 255:
                        return
                    mod.set_output_width(0, value)
                    mod.set_output_width(1, value)
                except ValueError:
                    self.publish("output_width", 0)
            elif prop == "majority_threshold":
                try:
                    mod.set_majority_threshold(value)
                except ValueError:
                    self.publish("majority_threshold", 0)

    def update(self, initial=False):
        pass


class Caen1471(USBAgent):
    SET_RE = re.compile(r".*\/channel([0-3]+)\/(.*)/set")
    ALLOWED_PROPERTIES = [
        "enabled",
        "voltage",
        "current_limit",
        "voltage_limit",
        "imon_range",
        "trip_time",
        "ramp_down_rate",
        "ramp_up_rate",
    ]

    def __init__(self, device_id, config):
        super().__init__(device_id, config)
        self._device = None

    @property
    def is_connected(self):
        return super().is_connected and self._device is not None and self._device.is_connected

    def connect(self):
        try:
            self._device = pycaen.Caen1471(
                self._device_config["path"], self._device_config["baudrate"]
            )
        except ConnectionError:
            self._device = None
            raise AgentError(f"Could not connect to {self._device_config['path']}")
        super().connect()

    def disconnect(self):
        if self._device:
            self._device.disconnect()
        super().disconnect()

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/#")

    def on_message(self, client, userdata, msg):
        if not self.is_connected:
            return
        if not (m := self.SET_RE.match(msg.topic)):
            return
        ch, prop = m.groups()
        if prop == "imon_range":
            try:
                value = pycaen.Caen1471.MonitoringRange(msg.payload.decode().upper())
            except ValueError:
                return
        else:
            try:
                value = float(msg.payload)
            except ValueError:
                print(f"Could not parse float from {msg.payload}")
                return
        hv = self._device
        channel = hv.channels[int(m.group(1))]
        prop = m.group(2)
        if prop not in self.ALLOWED_PROPERTIES:
            return
        setattr(channel, prop, value)
        if prop == "enabled":
            return
        new_value = getattr(channel, prop)
        if prop == "imon_range":
            new_value = new_value.value
        if new_value != value:
            self.publish(msg.topic.replace("/set", ""), new_value, raw_topic=True)

    def update(self, initial=False):
        if not self.is_connected:
            raise AgentError("Device is not connected")
        hv = self._device
        try:
            for ch in hv.channels:
                self.publish(f"channel{ch.channel}/measured_voltage", ch.measured_voltage)
                self.publish(f"channel{ch.channel}/measured_current", ch.measured_current)
                self.publish(f"channel{ch.channel}/status", ch.status.name)

            if initial:
                for ch in hv.channels:
                    ch_stub = f"channel{ch.channel}"
                    self.publish(f"{ch_stub}/enabled", int(ch.enabled), retain=True)
                    self.publish(f"{ch_stub}/voltage", ch.voltage, retain=True)
                    self.publish(f"{ch_stub}/current_limit", ch.current_limit, retain=True)
                    self.publish(f"{ch_stub}/voltage_limit", ch.voltage_limit, retain=True)
                    self.publish(f"{ch_stub}/trip_time", ch.trip_time, retain=True)
                    self.publish(f"{ch_stub}/ramp_down_rate", ch.ramp_down_rate, retain=True)
                    self.publish(f"{ch_stub}/imon_range", ch.imon_range.value, retain=True)
                    self.publish(f"{ch_stub}/ramp_up_rate", ch.ramp_up_rate, retain=True)
                    self.publish(f"{ch_stub}/polarity", ch.polarity, retain=True)
        except SerialException:
            self.disconnect()
            raise AgentError("Device is not connected")
