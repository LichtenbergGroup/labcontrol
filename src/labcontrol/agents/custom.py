#!/usr/bin/env python3
import re
import pyvme
from .agent import VMEAgent


class LichtenlabTrigger(VMEAgent):
    TRIGGER_EVENT_RE = re.compile(r".*\/([^\/]*)/trigger")
    SET_CHANNEL_RE = re.compile(r".*\/and_([1-4])\/(.*)/set")
    SET_DEVICE_RE = re.compile(r".*\/(.*)/set")

    def __init__(self, device_id, config, vme_controller=None):
        super().__init__(device_id, config, vme_controller)
        self._update_enable = True

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/#")

    def on_message(self, client, userdata, msg):
        prop = None
        channel = None
        event = None
        if m := self.SET_CHANNEL_RE.match(msg.topic):
            channel, prop = m.groups()
            channel = int(channel)
        elif m := self.SET_DEVICE_RE.match(msg.topic):
            channel = None
            prop = m.group(1)
        elif m := self.TRIGGER_EVENT_RE.match(msg.topic):
            event = m.group(1)
        else:
            return

        if prop == "update":
            if msg.payload == b"1":
                self._update_enable = True
                new_value = 1
            else:
                self._update_enable = False
                new_value = 0
            self.publish(msg.topic.replace("/set", ""), new_value, raw_topic=True, retain=True)

        with pyvme.modules.V2495(
            self._controller, self._device_config["vme_address"]
        ) as t:
            if prop == "mask" and channel is not None:
                t.write(
                    0x4000 + 4 * (channel - 1), int(msg.payload), pyvme.DataWidth.D32
                )
                new_value = t.read(0x4000 + 4 * (channel - 1), pyvme.DataWidth.D32)
            elif prop == "output_0_source":
                t.write(0x4010, int(msg.payload))
                new_value = t.read(0x4010)
            elif prop == "output_1_source":
                t.write(0x4014, int(msg.payload))
                new_value = t.read(0x4014)
            elif event == "reset_counters":
                t.write(0x10F0, 1, pyvme.DataWidth.D8)
                return
            else:
                return

        self.publish(msg.topic.replace("/set", ""), new_value, raw_topic=True, retain=True)

    def update(self, initial=False):
        if not self._update_enable:
            return
        with pyvme.modules.V2495(
            self._controller, self._device_config["vme_address"]
        ) as t:
            for input_channel in range(4):
                self.publish(
                    f"input_{input_channel}/counts",
                    int(t.read(0x1000 + 4 * input_channel, pyvme.DataWidth.D32)),
                )
            for and_channel in range(1, 5):
                self.publish(
                    f"and_{and_channel}/counts",
                    int(t.read(0x2000 + (and_channel - 1) * 4, pyvme.DataWidth.D32)),
                )
