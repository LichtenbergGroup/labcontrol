import socket
from struct import pack, unpack
from enum import Enum, IntEnum
from ipaddress import ip_address


class SRSPeripheral(IntEnum):
    SYS = 6007
    APVAPP = 6039
    APV = 6263
    ADCCARD = 6519


class SRSClient:
    UDP_SOURCE_PORT = 6007
    UDP_PORTS = {SRSPeripheral.APVAPP: 6039}

    class TriggerMode(Enum):
        RANDOM = 0
        EXTERNAL = 4

    def __init__(self, udp_ip):
        self.__udp_ip = udp_ip
        self.__udp_ip_int = int(ip_address(udp_ip))
        self._request_id = 0x80000000

    def __get_socket(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(("", SRSClient.UDP_SOURCE_PORT))
        return sock

    def _send_packet(self, peripheral, content):
        sock = self.__get_socket()
        sock.sendto(content, (self.__udp_ip, peripheral.value))

    def _send_packet_and_read(self, peripheral, content):
        sock = self.__get_socket()
        sock.sendto(content, (self.__udp_ip, peripheral.value))
        reply, sock_info = sock.recvfrom(1024)
        package = {}
        (
            package["request_id"],
            package["subaddress"],
            package["cmd_1"],
            package["cmd_2"],
        ) = unpack("!4I", reply[:16])
        data_len = int((len(reply) - 16) / 4)
        package["data"] = unpack(f"!{data_len}I", reply[16:])
        return package

    @property
    def is_connected(self):
        """Check if the SRS is available by reading the FEC IP address and comparing to the
        expected value."""
        reply = self.read_single(SRSPeripheral.SYS, 0x03)
        return reply == self.__udp_ip_int

    @property
    def next_request_id(self):
        self._request_id += 1
        return self._request_id

    def read_list(self, peripheral, *addresses):
        package = pack(
            f"!4I{len(addresses)}I",
            self.next_request_id,
            0x0,
            0xBBAAFFFF,
            0x0,
            *addresses,
        )
        reply = self._send_packet_and_read(peripheral, package)
        return {x: y for x, y in zip(addresses, reply["data"][1::2])}

    def read_burst(self, peripheral, first_address, length):
        dummy_data = length * [0]
        package = pack(
            f"!4I{length:.0f}I",
            self.next_request_id,
            0x0,
            0xBBBBFFFF,
            first_address,
            *dummy_data,
        )
        reply = self._send_packet_and_read(peripheral, package)
        return {first_address + i: value for i, value in enumerate(reply["data"][1::2])}

    def read_single(self, peripheral, address):
        return self.read_list(peripheral, address)[address]

    def write(self, peripheral, data, subaddress=0x00):
        if len(data) % 2 != 0:
            raise ValueError("Write pairs only takes pairs of address + data")
        package = pack(
            f"!4I{len(data)}I", self.next_request_id, subaddress, 0xAAAAFFFF, 0x0, *data
        )
        return self._send_packet_and_read(peripheral, package)

    def init(self):
        self._init_adc_card()
        self._init_apv()
        self._init_apvapp()

    def _init_adc_card(self):
        # fmt: off
        self.write(SRSPeripheral.ADCCARD, [
            0x00, 0,
            0x01, 0,
            0x02, 0,
            0x03, 0,
            0x04, 0,
            0x05, 0,
            0x06, 0xFF,
            0x00, 0xFF,
        ])
        # fmt: on

    def _init_apv(self, values=None):
        values = values or {}
        # fmt: off
        self.write(SRSPeripheral.APV, [
            0x10, int(values.get("IPRE", 100)),
            0x11, int(values.get("IPCASC", 60)),
            0x12, int(values.get("IPSF", 34)),
            0x13, int(values.get("ISHA", 34)),
            0x14, int(values.get("ISSF", 34)),
            0x15, int(values.get("IPSP", 55)),
            0x16, int(values.get("IMUXIN", 10)),
            0x17, 0,
            0x18, int(values.get("ICAL", 100)),
            0x19, int(values.get("VPSP", 30)),
            0x1A, int(values.get("VFS", 60)),
            0x1B, int(values.get("VFP", 40)),
            0x1C, int(values.get("CDRV", 239)),
            0x1D, int(values.get("CSEL", 254)),
            0x01, int(values.get("MODE", 25)),
            0x02, int(values.get("LATENCY", 132)),
            0x03, int(values.get("MUXGAIN", 4)),
        ], subaddress=0xFFFFFFFF)
        # fmt: on

    def _init_apvapp(self, values=None):
        self.write(SRSPeripheral.APVAPP, [0xFFFFFFFF, 0x01])

    @property
    def ro_enable(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x0F)

    @ro_enable.setter
    def ro_enable(self, value):
        return self.write(SRSPeripheral.APVAPP, [0x0F, value])

    @property
    def bclk_mode(self):
        return self.read_single(SRSPeripheral.APVAPP, 0)

    @bclk_mode.setter
    def bclk_mode(self, value):
        return self.write(SRSPeripheral.APVAPP, [0, value])

    @property
    def trigger_mode(self):
        return SRSClient.TriggerMode(self.bclk_mode)

    @trigger_mode.setter
    def trigger_mode(self, value):
        if type(value) == SRSClient.TriggerMode:
            value = value.value
        self.bclk_mode = value

    @property
    def bclk_trgburst(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x01)

    @bclk_trgburst.setter
    def bclk_trgburst(self, value):
        self.write(SRSPeripheral.APVAPP, [0x01, value])

    @property
    def num_timesteps(self):
        return (self.bclk_trgburst + 1) * 3

    @num_timesteps.setter
    def num_timesteps(self, value):
        if value % 3 != 0:
            raise ValueError(
                "num_timesteps must be a multiple of 3 (it will be converted automatically)"
            )
        self.bclk_trgburst = int(value / 3 - 1)

    @property
    def bclk_freq(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x02)

    @bclk_freq.setter
    def bclk_freq(self, value):
        self.write(SRSPeripheral.APVAPP, [0x02, int(value)])

    @property
    def bclk_trgdelay(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x03)

    @bclk_trgdelay.setter
    def bclk_trgdelay(self, value):
        self.write(SRSPeripheral.APVAPP, [0x03, int(value)])

    @property
    def bclk_tpdelay(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x04)

    @bclk_tpdelay.setter
    def bclk_tpdelay(self, value):
        self.write(SRSPeripheral.APVAPP, [0x04, int(value)])

    @property
    def bclk_rosync(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x05)

    @bclk_rosync.setter
    def bclk_rosync(self, value):
        self.write(SRSPeripheral.APVAPP, [0x05, int(value)])

    @property
    def evbld_chenable(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x08)

    @evbld_chenable.setter
    def evbld_chenable(self, value):
        self.write(SRSPeripheral.APVAPP, [0x08, int(value)])

    @property
    def evbld_datalength(self):
        return self.read_single(SRSPeripheral.APVAPP, 0x09)

    @evbld_datalength.setter
    def evbld_datalength(self, value):
        self.write(SRSPeripheral.APVAPP, [0x09, int(value)])
