#!/usr/bin/env python3
import re
from .agent import Agent, AgentError
from serial import Serial


class MKSPR4000Device:
    def __init__(self, path):
        self._serial = Serial(path, 115200, timeout=1)

    @property
    def is_connected(self):
        return self._serial.is_open

    def disconnect(self):
        if self._serial:
            self._serial.close()

    def get_status(self):
        return self.get_channel_enabled(1) and self.get_channel_enabled(2)

    def enable(self):
        self.channel_enable(1)
        self.channel_enable(2)

    def disable(self):
        self.channel_disable(1)
        self.channel_disable(2)

    def get_flow(self, channel):
        self._serial.write(f"?SP{channel}\r".encode())
        return float(self._serial.read_until(b"\r").decode().strip())

    def set_flow(self, channel, sp):
        self._serial.write(f"SP{channel},{sp:.2f}\r".encode())
        self._serial.read_until(b"\r")

    def get_measured_flow(self, channel):
        self._serial.write(f"?AV{channel}\r".encode())
        return float(self._serial.read_until(b"\r").decode().strip())

    def get_channel_enabled(self, channel):
        self._serial.write(f"?AC{channel}\r".encode())
        return self._serial.read_until(b"\r").decode().strip().split(",")[1] == "ON"

    def channel_enable(self, channel):
        self._serial.write(f"AC{channel},{self.get_flow(channel)},ON\r".encode())
        self._serial.read_until(b"\r")

    def channel_disable(self, channel):
        self._serial.write(f"AC{channel},{self.get_flow(channel)},OFF\r".encode())
        self._serial.read_until(b"\r")


class MKSPR4000(Agent):
    SET_CHANNEL_RE = re.compile(r".*\/(.*)\/(.*)/set")
    SET_MODULE_RE = re.compile(r".*\/(.*)/set")

    def __init__(self, device_id, config):
        super().__init__(device_id, config)
        self._device = None

    @property
    def is_connected(self):
        return super().is_connected and self._device is not None and self._device.is_connected

    def connect(self):
        try:
            self._device = MKSPR4000Device(self._device_config["path"])
        except ConnectionError:
            self._device = None
            raise AgentError(f"Could not find {self._device_config['path']}")
        super().connect()

    def disconnect(self):
        if self._device:
            self._device.disconnect()
        self._device = None
        super().disconnect()

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/+/+/set")
        client.subscribe(f"{self._device_id}/+/set")

    def on_message(self, client, userdata, msg):
        if not self.is_connected:
            print("not connected")
            return
        m = self.SET_CHANNEL_RE.match(msg.topic)
        if m:
            # Per channel settings
            ch, prop = m.groups()
        else:
            m = self.SET_MODULE_RE.match(msg.topic)
            if m:
                # Module settings
                ch = None
                prop = m.groups()[0]
            else:
                return

        if ch is None and prop not in ["enabled"]:
            return
        if ch is not None and prop not in ["flow"]:
            return
        if ch is not None and ch not in ["1", "2"]:
            return

        try:
            value = float(msg.payload)
        except ValueError:
            print(f"Could not parse float from {msg.payload}")
            return

        if ch is None:
            if prop == "enabled":
                if value == 0:
                    self._device.disable()
                elif value == 1:
                    self._device.enable()
        else:
            if prop == "flow":
                self._device.set_flow(ch, value)
        self.publish(msg.topic.replace("/set", ""), value, raw_topic=True)

    def update(self, initial=False):
        if not self.is_connected:
            raise AgentError("Device is not connected")
        try:
            for i in range(1, 3):
                self.publish(f"{i}/measured_flow", self._device.get_measured_flow(i))
            if initial:
                self.publish("enabled", int(self._device.get_status()), retain=True)
                for i in range(1, 3):
                    self.publish(f"{i}/flow", int(self._device.get_flow(i)), retain=True)
        except ValueError:
            self.publish("error", "Could not properly read from the device.")
            self.disconnect()
            raise AgentError("Device is not connected")
