from .libs.srs import SRSClient
from .agent import Agent, AgentError


class SRS(Agent):
    def __init__(self, device_id, config):
        super().__init__(device_id, config)
        self._device = None

    def connect(self):
        try:
            self._device = SRSClient(self._device_config["ip_address"])
            if not self._device.is_connected:
                raise ConnectionError()
        except ConnectionError:
            self._device = None
            raise AgentError(f"Could not connect to {self._device_config['ip_address']}")
        super().connect()

    def disconnect(self):
        self._device = None
        super().disconnect()

    @property
    def is_connected(self):
        return self._device is not None and self._device.is_connected

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(f"{self._device_id}/+/set")
        client.subscribe(f"{self._device_id}/+/trigger")

    def on_message(self, client, userdata, msg):
        _, prop, action = msg.topic.split("/")
        if not self.is_connected:
            return

        if action == "set":
            value = int(msg.payload)
            if prop == "trigger_mode":
                self._device.trigger_mode = SRSClient.TriggerMode(value)
            elif prop == "num_timesteps":
                self._device.num_timesteps = value
            elif prop == "trigger_frequency":
                self._device.bclk_freq = value
            elif prop == "trigger_delay":
                self._device.bclk_trgdelay = value
            elif prop == "testpulse_delay":
                self._device.bclk_tpdelay = value
            elif prop == "readout_sync":
                self._device.bclk_rosync = value
            elif prop == "channels_enabled":
                self._device.evbld_chenable = value
            elif prop == "datalength":
                self._device.evbld_datalength = value
            elif prop == "readout_enabled":
                self._device.ro_enable = (value != 0)
            else:
                return
            self.publish(msg.topic.replace("/set", ""), value, raw_topic=True, retain=True)
        elif action == "trigger":
            if prop == "readout_enable":
                self._device.ro_enable = True
            elif prop == "readout_disable":
                self._device.ro_enable = False
            else:
                return
            self.publish(msg.topic.replace("/trigger", ""), 1, raw_topic=True)

    def update(self, initial=False):
        return
