#!/usr/bin/python3
import os
from argparse import ArgumentParser
from pathlib import Path


if __name__ == "__main__":
    argparser = ArgumentParser(prog="labcontrol-web")
    argparser.add_argument(
        "-c",
        "--config",
        default=os.environ.get(
            "LAB_CONTROL_CONFIG", Path.home() / ".config/lab_control/web_config.yml"
        ),
        type=Path,
    )
    argparser.add_argument("-p", "--port", default=5001, type=int)
    options = argparser.parse_args()

    from .server import run_server

    run_server(config_filename=options.config, port=options.port)
