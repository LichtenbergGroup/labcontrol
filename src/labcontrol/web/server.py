#!/usr/bin/env pythone
from yaml import load, CLoader
from pathlib import Path
from flask import Flask
from flask import render_template


app = Flask(__name__)
app.jinja_options["extensions"] = ["jinja2.ext.do", "jinja2.ext.loopcontrols"]
app.config.from_pyfile("instance/config.py")


@app.route("/")
def root():
    return render_template(
        "main.html", config=config, devices=config.get("devices", {})
    )


def run_server(config_filename=None, port=5001):
    global config
    if config_filename is None:
        config_filename = Path.home() / ".config/lab_control/web_config.yml"
    config = load(Path(config_filename).open(), Loader=CLoader)
    app.run(host="0.0.0.0", port=port, debug=True, threaded=True)
