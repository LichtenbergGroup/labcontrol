var unit_re = /([\-0-9]*[.]{0,1}[0-9]+)[ ]*([A-Za-z]*)[/div]*/;
var unit_prefixes = {"P": 1e12, "G": 1e9, "M": 1e6, "k": 1e3, "m": 1e-3, "u": 1e-6, "n": 1e-9, "p": 1e-12};

/**
 * Convert a string with unit to a float
 */
function unit_string_to_float(str, ignore_prefix=false) {
  let [_, value, unit] = unit_re.exec(str);
  if (unit == "undefined") {
    unit = "";
  }

  if (ignore_prefix) {
    return parseFloat(value);
  }

  // Parse the value and multiply with the according factor if it is known
  return parseFloat(value) * ((unit[0] in unit_prefixes) ? unit_prefixes[unit[0]] : 1);
}

/**
 * Convert a float to a unit string
 */
function float_to_unit_string(value, suffix, dec=0, disable_prefix=false) {
  value = parseFloat(value);
  let abs_value = Math.abs(value);

  // If prefix should be ignored, don't try to find it
  if (!disable_prefix) {
    for (let prefix of Object.keys(unit_prefixes)) {
      if ((abs_value / unit_prefixes[prefix] >= 1) && (abs_value / unit_prefixes[prefix] < 1000)) {
        return (value / unit_prefixes[prefix]).toFixed(dec) + " " + prefix + suffix;
      }
    }
  }
  return value.toFixed(dec) + ((suffix != "") ? " " + suffix : "");
}

/**
 * Update a standard property field
 */
function update_property(path, value) {
  let $el = $(`[data-path='${path}']`);
  // Check if it is in focus and if so, don't update
  if ($el.is(":focus") || $el.data("no-update")) {
    return;
  }
  if (($el == "undefined") || ($el.length == 0)) {
    return;
  }

  let tag = $el.prop("tagName").toLowerCase();
  var type = "";
  try {
    type = $el.prop("type").toLowerCase();
  } catch (e) {
  }

  // Select field
  if (tag == "select") {
    $el.find("option").removeAttr("selected");
    let new_option = $el.find("option[value='" + value + "']");
    new_option.attr("selected", true);
    $(M.FormSelect.getInstance($el).input).val(new_option.html());
    return;
  }

  // Range field
  if ((tag == "input") && (type == "range")) {
    if ($el.data("scale")) {
      value = parseFloat(value) / $el.data("scale");
    }
    $el.val(value);
    return;
  }

  // Checkbox
  if ((tag == "input") && (type == "checkbox")) {
    $el[0].checked = (value > 0) || (value.toLowerCase() == "true");
    return;
  }

  // Flag
  if ((tag == "input") && (type == "text") && ($el.hasClass("flag"))) {
    $el.attr("value", value);
    $el.val(value);
    return;
  }

  // Text field
  if ((tag == "input") && (type == "text")) {
    // Check for scale
    if ($el.data("scale")) {
      value = parseFloat(value) * parseFloat($el.data("scale"));
    }

    // Have to take unit into account
    let dec = $el.data("dec");
    if (dec != undefined) {
      value = parseFloat(value).toFixed(dec);
    }
    if ($el.data("unit")) {
      value = `${value} ${$el.data("unit")}`;
    }
    $el.val(value);
    return;
  }

  // Other
  $el.attr("data-value", value);
  if ($el.hasClass("tooltipped") || $el.hasClass("hv-status")) {
    $el.attr("data-tooltip", value);
  }
  return;

  console.error("Could not update " + name + " of type " + type + ", because the option does not exist: " + value);
}

/**
 * Update a bit array field -> multiple checkboxes
 */
function update_bit_property(path, value) {
  var $els = $(`.bit-property[data-path='${path}']`);
  for (let el of $els) {
    el.checked = (value & (1 << $(el).data("bit")));
  }
}

function get_bit_property_value(path) {
  let value = 0;
  document.querySelectorAll(`.bit-property[data-path='${path}']`).forEach((el) => {
    if (el.checked) {
      value |= (1 << el.getAttribute("data-bit"));
    }
  });
  return value;
}
