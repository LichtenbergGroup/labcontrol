(function ($) {
  $.fn.replaceWithPush = function(a) {
      var $a = $(a);
      this.replaceWith($a);
      return $a;
  };
  $.fn.fancyInputs = function() {
    return this.each(function() {
      let $this = $(this);
      var input_type = this.tagName.toLowerCase();
      if ($this.attr("type")) {
        input_type += "." + $this.attr("type").toLowerCase();
      }
      let with_range = $this.hasClass("range");

      // Create the surrouding tags
      if (with_range) {
        var outer_div = $('<div class="range-input"></div>');
        var btn_minus = $('<a class="btn-floating waves-effect waves-light red btn-small"><i class="material-icons">remove</i></a>');
        var btn_plus = $('<a class="btn-floating waves-effect waves-light red btn-small"><i class="material-icons">add</i></a>');
      }
      var input_div = $('<div class="fancy-input"></div>');

      if (with_range) {
        outer_div.append(btn_minus);
      }
      input_div.append($this.clone());
      if (input_type == "input.text") {
        var edit_control_left = $('<div class="edit-control left hide"><a class="abort" href="#"><i class="material-icons">undo</i></a></div>');
        var edit_control_right = $(
          '<div class="edit-control right hide"><a class="confirm" href="#"><i class="material-icons">check_circle</i></a></div>'
        );
        input_div.append(edit_control_left);
        input_div.append(edit_control_right);
      }
      if (with_range) {
        outer_div.append(input_div);
        outer_div.append(btn_plus);
        $this = $this.replaceWithPush(outer_div);
      } else {
        $this = $this.replaceWithPush(input_div);
      }

      if (input_type == "input.text") {
        // Handle the +/- buttons
        $this.find(".btn-floating").click(function(e) {
          let $el = $(e.target).closest(".range-input").find("input");
          $el.trigger("edit:before");
          let dir = (($(e.target).html().indexOf("add") >= 0) ? 1 : -1);
          let [value, unit] = $el.val().split(" ");
          value = parseFloat(value);
          let dec = $el.data("dec") ? $el.data("dec") : 0;

          if ($el.data("step")) {
            value = unit_string_to_float(value + " " + unit);
            value += dir * $el.data("step");
            if ($el.data("min-value") && (value < $el.data("min-value"))) {
              value = $el.data("min-value");
            }
            if ($el.data("max-value") && (value > $el.data("max-value"))) {
              value = $el.data("max-value");
            }
            $el.val(float_to_unit_string(value, $el.data("unit"), dec, $el.data("disable-prefix") == "yes"));
          } else {
            let log_value = Math.floor(Math.log10(value));
            let step = dir * Math.pow(10, log_value);
            if ((dir < 0) && (log_value == Math.log10(value))) {
              step /= 10;
            }
            if (step == 0) {
              step = dir;
            }
            value = unit_string_to_float((value + step) + " " + unit);
            if ($el.data("min-value") && (value < $el.data("min-value"))) {
              value = $el.data("min-value");
            }
            if ($el.data("max-value") && (value > $el.data("max-value"))) {
              value = $el.data("max-value");
            }
            $el.val(float_to_unit_string(value, $el.data("unit")));
          }
          $el.focus();
        });

        // Handle showing the edit-controls and enter
        var $input = $this.find("input");
        var $edit_controls = $this.find(".edit-control");
        $input.keydown(function(event) {
          $input.trigger("edit:before");
        });
        $input.keyup(function(event) {
          if (event.keyCode === 13) {
            $(this).trigger("edit:confirm");
          } else if ((event.keyCode === 27) || (event.ctrlKey && (event.key === "c"))) {
            $(this).trigger("edit:abort");
          }
        });

        // Handle editing events
        $input.on("edit:before", function() {
          let $this = $(this);
          let $edit_controls = $this.closest(".fancy-input").find(".edit-control");
          if (!($this.data("last-value"))) {
            $this.data("last-value", $this.val());
            $edit_controls.removeClass("hide");
          }
          $this.data("no-update", true);
        });
        $input.on("edit:abort", function() {
          let $this = $(this);
          let $edit_controls = $this.closest(".fancy-input").find(".edit-control");
          if ($this.data("last-value")) {
            $this.val($this.data("last-value"));
          }
          $this.data("last-value", null);
          $this.data("no-update", false);
          $edit_controls.addClass("hide");
          $this.blur();
        });
        $input.on("edit:confirm", function() {
          let $this = $(this);
          let $edit_controls = $this.closest(".fancy-input").find(".edit-control");
          $edit_controls.addClass("hide");
          $this.data("last-value", null);
          $this.data("no-update", false);
          $this.blur();
          $this.trigger("updated");
        });

        // Handle edit controls
        $edit_controls.find("a.abort").click(function(e) {
          let $el = $(e.target).closest(".fancy-input").find("input");
          $el.trigger("edit:abort");
        });
        $edit_controls.find("a.confirm").click(function(e) {
          let $el = $(e.target).closest(".fancy-input").find("input");
          $el.trigger("edit:confirm");
        });

      } else if (input_type == "select") {
        $this.click(function(e) {
          let $el = $(e.target).closest(".range-input").find("select");
          let dir = (($(e.target).html().indexOf("add") >= 0) ? 1 : -1);
          let new_index = $el[0].selectedIndex + dir;

          if ((new_index >= 0) && (new_index < $el[0].options.length)) {
            $el[0].selectedIndex = new_index;
            $el.formSelect();
            $el.trigger("updated");
          }
        });
      } else if (input_type == "input.range") {
        $this.click(function(e) {
          var $el = $(e.target).closest(".range-input").find("input");
          var dir = (($(e.target).html().indexOf("add") >= 0) ? 1 : -1);
          $el.val(parseFloat($el.val()) + dir);
          $el.trigger("updated");
        });
      }
    });
  };
}(jQuery));
