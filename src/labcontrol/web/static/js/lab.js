// Generate or read client id
let client_id = "lc_" + Math.random().toString(36).substr(2, 9);

// Connect to MQTT Server and register callbacks
var client = new Paho.Client(
  config["mqtt"]["hostname"],
  Number(config["mqtt"]["port"]),
  client_id
);
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

// If multiple URIs for MQTT are given use them
if ("uris" in config["mqtt"]) {
  client.connect({uris: config["mqtt"]["uris"], onSuccess:onConnect});
} else {
  client.connect({onSuccess:onConnect});
}


// Callback for sucessful connection
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  for (let device of config["devices"]) {
    client.subscribe(`${device["id"]}/#`);
    console.log(`Subscribed to ${device["id"]}/#`);
  }
}

// Callback for when the connection is lost
function onConnectionLost(response) {
  if (response.errorCode !== 0) {
    M.toast({
      html: `The connection to MQTT is lost with the following error message:<br />${response.errorMessage}`
    });
  }
}

// Callback for when a new message arrives
function onMessageArrived(msg) {
  var $el;

  $("#last-updated").html((new Date()).toLocaleTimeString("DE"));

  // Update standard property if available
  $el = $(`.property[data-path='${msg.destinationName}'],.output[data-path='${msg.destinationName}']`);
  if ($el.length > 0) {
    update_property(msg.destinationName, msg.payloadString);
    return;
  }

  let msg_topic = msg.destinationName.replace("/set", "");
  $el = $(`.property[data-path='${msg_topic}'],.output[data-path='${msg_topic}']`);
  if ($el.length > 0) {
    update_property(msg_topic, msg.payloadString);
    return;
  }

  // Update property from a bit array -> multiple checkboxes
  $el = $(`.bit-property[data-path='${msg.destinationName}']`);
  if ($el.length > 0) {
    update_bit_property(msg.destinationName, msg.payloadString);
    return;
  }

  console.log(msg.destinationName, msg.payloadString);
}

function send_button_press($el) {
  let path = $el.data("path");
  message = new Paho.Message("1");
  message.retained = false;
  message.destinationName = `${path}/trigger`;
  client.send(message);
}

function send_input_update($el) {
  let path = $el.data("path");
  let value = 0;
  if ($el[0].type == "checkbox") {
    value = ($el[0].checked ? "1" : "0");
  } else {
    value = $el.val();
    if ($el.data("scale") != undefined) {
      value = parseFloat(value) * $el.data("scale");
    }
    if ($el.data("unit") != undefined) {
      let ignore_prefix = ($el.data("disable-prefix") == "yes");
      value = unit_string_to_float(value, ignore_prefix);
    }
    value = value.toString();
  }

  message = new Paho.Message(value);
  message.retained = true;
  message.destinationName = `${path}/set`;
  client.send(message);
}

function send_bit_property_update($el) {
  let path = $el.data("path");
  let value = 0;
  if ($el[0].checked) {
    value |= (1 << $el.data("bit"));
  }
  var $els = $(`.bit-property[data-path='${path}']:not([data-bit='${$el.data("bit")}'])`);
  for (let el of $els) {
    if (el.checked) {
      value |= (1 << $(el).data("bit"));
    }
  }
  value = value.toString();

  message = new Paho.Message(value);
  message.retained = false;
  message.destinationName = `${path}/set`;
  client.send(message);
}

var loaded_config = "";

$(document).ready(function() {
  if ($(".tabs li").length > 0) {
    $(".tabs").tabs();
  }

  // Make sure the last tab is shown on refresh!
  if (location.hash !== "") {
    $(".tabs").tabs("select", location.hash);
  }
  $("li.tab a").click(function(e) {
    if (history.replaceState) {
      history.replaceState(null, null, "#" + $(e.target).attr("href").substr(1));
    } else {
      location.hash = "#" + $(e.target).attr("href").substr(1);
    }
    $(window).trigger("hashchange");
  });

  $(".tooltipped").tooltip({enterDelay: 400, margin: -5, transitionMovement: 5});
  $(".hv-status").tooltip();
  $("input.property[type!=checkbox],select.range").fancyInputs();
  $("select").formSelect();

  $("a.btn.trigger").click(function() {
    send_button_press($(this));
  });
  $("input.property[type=checkbox]").change(function() {
    send_input_update($(this));
  });
  $("input.property[type!=checkbox],select.property").on("change", function(event) {
    send_input_update($(this));
  });
  $("input.bit-property[type=checkbox]").change(function() {
    send_bit_property_update($(this));
  });
  M.Range.init($('input[type=range]'))

  // Setup modals
  $('.modal').modal();
  $("#load-config-modal input[name='filename']").on("change", event => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event) => {
      const result = event.target.result;
      loaded_config = jsyaml.load(result);
      // Do something with result
    });

    reader.readAsText(event.target.files[0]);
  });
  $("#load-config-modal a.load").click(function event() {
    console.log("Would load this config now:");
    console.log(loaded_config);
  });
  //   console.log($("#load-config-modal input[name='filename']").val());
  //   const reader = new FileReader();
  //   reader.addEventListener('load', (event) => {
  //     const result = event.target.result;
  //     console.log(result);
  //     // Do something with result
  //   });

  //   reader.addEventListener('progress', (event) => {
  //     if (event.loaded && event.total) {
  //       const percent = (event.loaded / event.total) * 100;
  //       console.log(`Progress: ${Math.round(percent)}`);
  //     }
  //   });
  //   reader.readAsDataURL($("#load-config-modal input[name='filename']").val());
  // });
});
