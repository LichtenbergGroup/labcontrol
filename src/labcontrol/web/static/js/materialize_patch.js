// Patch Materialize tooltips to allow HTML
M.Tooltip.prototype._setTooltipContent = function(tooltipContentEl) {
  tooltipContentEl.innerHTML = this.options.text;
  if (!!this.options.html) {
    // Warn when using html
    console.warn(
      'The html option is deprecated and will be removed in the future. See https://github.com/materializecss/materialize/pull/49'
    );
    $(tooltipContentEl).append(this.options.html);
  }
  if (!!this.options.unsafeHTML) {
    $(tooltipContentEl).append(this.options.unsafeHTML);
  }
};
