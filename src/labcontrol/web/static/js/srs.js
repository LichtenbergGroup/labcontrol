const SRS_CONFIG_DEFAULT = {
  trigger_mode: 0,
  num_timesteps: 27,
  trigger_delay: 67,
  testpulse_delay: 128,
  readout_delay: 300,
  channels_enabled: 63,
};
const SRS_INITIALIZE_PROPERTIES = [
  "trigger_mode", "num_timesteps", "trigger_delay", "testpulse_delay", "readout_delay", "datalength", "channels_enabled"
];


function parse_config(content) {
  let config = {
    ...SRS_CONFIG_DEFAULT,
    ...JSON.parse(content),
  };
  return config;
}


function read_srs_config(e) {
  let target = e.target;
  const stub = e.target.getAttribute("data-stub");
  let file = target.files[0];
  if (!file) {
    return;
  }

  let reader = new FileReader();
  reader.onload = (e) => {
    const contents = e.target.result;

    const config = parse_config(contents);
    for (const [key, value] of Object.entries(config)) {
      const el = document.querySelector(`[data-path="${stub}/${key}"]`);
      if (el === null) {
        continue;
      }
      if (el.classList.contains("bit-property")) {
        update_bit_property(`${stub}/${key}`, value);
        continue
      }
      el.value = value;

      var event = document.createEvent("HTMLEvents");
      event.initEvent("change", false, true);
      el.dispatchEvent(event);
    }
  };
  reader.readAsText(file);
}

function save_srs_config(stub) {
  let config = {...SRS_CONFIG_DEFAULT};
  for (const [key, value] of Object.entries(config)) {
    const el = document.querySelector(`[data-path="${stub}/${key}"]`);
    if (el === null) {
      continue;
    }
    if (el.classList.contains("bit-property")) {
      config[key] = get_bit_property_value(`${stub}/${key}`, value);
      continue
    }
    config[key] = el.value;
  }

  var blob = new Blob([JSON.stringify(config)], {type: "text/json"});
    const a = document.createElement('a');
    a.download = 'srs_config.json';
    a.href = URL.createObjectURL(blob);
    a.addEventListener('click', (e) => {
      setTimeout(() => URL.revokeObjectURL(a.href), 30 * 1000);
    });
    a.click();
}


document.addEventListener("DOMContentLoaded", (e) => {
  document.querySelectorAll("[data-action='srs-initialize']").forEach((el) => {
    el.addEventListener("click", (e) => {
      // Trigger initialize
      let stub = e.currentTarget.getAttribute("data-stub");
      message = new Paho.Message("{}");
      message.destinationName = `${stub}/initialize/trigger`;
      client.send(message);

      // Send APVAPP settings
      for (let prop of SRS_INITIALIZE_PROPERTIES) {
        let el = document.querySelector(`[data-path="${stub}/${prop}"]`);
        if (el.classList.contains("bit-property")) {
          send_bit_property_update($(el));
          continue
        }
        send_input_update($(el));
      }
    });
  });

  document.querySelectorAll(".srs-file-select").forEach((el) => {
    el.addEventListener("change", read_srs_config, false);
  });
  document.querySelectorAll("[data-action='srs-load-config']").forEach((el) => {
    el.addEventListener("click", (e) => {
      document.getElementById(`${e.currentTarget.getAttribute("data-stub")}-file-select`).click();
    });
  });
  document.querySelectorAll("[data-action='srs-save-config']").forEach((el) => {
    el.addEventListener("click", (e) => { console.log(e.currentTarget); save_srs_config(e.currentTarget.getAttribute("data-stub")) });
  });
});
